// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract Lottary{
    address public owner;
    address payable[] public players;

    constructor(){
        owner= msg.sender;
    }
    receive() payable external{
        require(msg.value == 5 ether);
        players.push(payable(msg.sender));
    }

    function getBalance() public view returns(uint){
        require(msg.sender == owner);
        return address(this).balance;
    }
  
    function winner() public {
      require(msg.sender == owner);
      address payable wineradd;
      uint r = uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp, players.length)));
      uint index = r % players.length;
      wineradd= players[index];
      uint OwnerProfit = getBalance() *  7/ 100; 
      uint sendMoney = getBalance() - OwnerProfit;
      wineradd.transfer(sendMoney);
      
      players = new address payable[](0);
    }
}